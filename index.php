<?php
require_once (__DIR__.'/libs/crest/CRestPlus.php');
require_once (__DIR__.'/libs/functions.php');
define ('CLIENT', __DIR__.'/libs/crest/settings.json');
define ('LISTS_PARAM', array('IBLOCK_TYPE_ID' => 'lists', 'IBLOCK_ID' => '52')); // id списка
$begin_month = date('Y-m-d',strtotime('today - '.date('d').'day + 1day'));      // 1 число текущего месяца
$end_month = date('Y-m', strtotime('today')).'-'.date('t', strtotime('01-11-2019'));
$limit = strtotime('today') - strtotime('-3 year');                            // > 3лет == сделка первичная
#=========================================== settings ================================================#
if (isset($_REQUEST['DOMAIN']) && !file_exists(CLIENT)) { $install = CRestPlus::installApp(); }
$currency = callMoney(date('d/m/Y'))['Valute'][11]['Value'];
if (isset($_POST['send'])) {
	$begin_month = ($_POST['year'] ?: date('Y')).'-'.($_POST['month'] ?: date('m')).'-01';
	$tmp_time = date('t', strtotime('01-'.($_POST['month'] ?: date('m')).'-'.($_POST['year'] ?: date('Y'))));
	$end_month = ($_POST['year'] ?: date('Y')).'-'.($_POST['month'] ?: date('m')).'-'.date('t', strtotime('01-11-2019'));
}
### получаем список ###
$lists = CRestPlus::callBatchList ('lists.element.get', LISTS_PARAM);
foreach ($lists['result']['result'] as $list) {
	foreach ($list as $value) {
		$ids[] = current($value['PROPERTY_237']);
		$manager = CRestPlus::callBatchUsers ($ids);
		$mainArr[current($value['PROPERTY_237'])] = array(
			'plan'      => current($value['PROPERTY_238']),
			'marg_more' => current($value['PROPERTY_241']),
			'marg_less' => current($value['PROPERTY_242']),
			'marg_gen'  => current($value['PROPERTY_243'])
		);
		### подсчет дел менеджера ###
		$activity[current($value['PROPERTY_237'])] = CRestPlus::callBatchList ('crm.activity.list', array(
			'filter' => array('RESPONSIBLE_ID' => current($value['PROPERTY_237']),'COMPLETED' => 'Y',
				'>=END_TIME' => $begin_month, 'TYPE_ID' => '1'), 'select' => array('ID')))['result']['result'];
		foreach ($activity[current($value['PROPERTY_237'])] as $v) {
			foreach ($v as $id) { $mainArr[current($value['PROPERTY_237'])]['out'] += 1; }
		}
	}
}
foreach ($manager['result'] as $value) {
	foreach ($value as $v) { $mainArr[$v['ID']]['manager'] = $v['LAST_NAME'].' '.$v['NAME']; }
}

### получаем сделки по этому менеджеру ###
foreach (array_keys($mainArr) as $v) {
	$dealsData[] = array( 'method' => 'crm.deal.list', 'params' => array(
		'FILTER' => array('ASSIGNED_BY_ID' => $v, 'STAGE_ID' => array('WON', 'C1:WON'), '>=CLOSEDATE' => $begin_month, '<=CLOSEDATE' => $end_month),
		'SELECT' => array('CLOSEDATE', 'CURRENCY_ID', 'CONTACT_ID', 'COMPANY_ID', 'ASSIGNED_BY_ID', 'OPPORTUNITY', 'UF_*'))
	);
}
$dealsData = array_chunk($dealsData, 50);
for ($i = 0, $s = count($dealsData); $i < $s; $i++) { $deals = CRestPlus::callBatch ($dealsData[$i]); }
foreach ($deals['result']['result'] as $deal) {
	foreach ($deal as $v) {
		if (!$v['COMPANY_ID'] && !$v['CONTACT_ID']) continue;
		$repeateArr[$v['ASSIGNED_BY_ID']][$v['COMPANY_ID'] ? 'company' : 'contact'][$v['COMPANY_ID'] ?: $v['CONTACT_ID']][] = $v['CLOSEDATE'];
	}
}

### собираем данные по сделкам ###
foreach ($deals['result']['result'] as $deal) {
	foreach ($deal as $v) {
		$i = 0;
		if ($v['CURRENCY_ID'] == 'EUR') $money = $v['OPPORTUNITY'] * $currency;
		else $money = $v['OPPORTUNITY'];
		$mainArr[$v['ASSIGNED_BY_ID']]['opportunity']['closed'] += round($money);
		$mainArr[$v['ASSIGNED_BY_ID']]['agent'] += $v['UF_CRM_1579878625066'];
		$mainArr[$v['ASSIGNED_BY_ID']]['cost_services'] += $v['UF_CRM_1579946476507'];
		$mainArr[$v['ASSIGNED_BY_ID']]['services'] += $v['UF_CRM_1579946502838'];
		if (isset($v['UF_CRM_1579878704'])) { $mainArr[$v['UF_CRM_1579878704']]['co_executor'] += $money; } // соисполнитель
		if (isset($v['UF_CRM_1579878727'])) { $mainArr[$v['UF_CRM_1579878727']]['curator'] += $money; } // куратор
		$tmpRepeate = array_values($repeateArr[$v['ASSIGNED_BY_ID']]['company']);
		usort($tmpRepeate, function ($a, $b) { if ($a > $b) return 1; });
		$limit_dif = strtotime($tmpRepeate[$i+1]) - strtotime($tmpRepeate[$i]);
		if (count($repeateArr[$v['ASSIGNED_BY_ID']]['company'][$v['COMPANY_ID']]) > 1 || count($repeateArr[$v['ASSIGNED_BY_ID']]['contact'][$v['CONTACT_ID']]) > 1) {
			if ($limit_dif > $limit) $mainArr[$v['ASSIGNED_BY_ID']]['opportunity']['second'] += round($money);
			else $mainArr[$v['ASSIGNED_BY_ID']]['opportunity']['second'] += round($money);
		} else { $mainArr[$v['ASSIGNED_BY_ID']]['opportunity']['first'] += round($money); }
		$i++;
	}
}
require_once (__DIR__.'/view/index.php');