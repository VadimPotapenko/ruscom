<?php
require_once (__DIR__.'/libs/crest/CRestPlus.php');
require_once (__DIR__.'/libs/debugger/Debugger.php');
// обновление данных по маржинальности
if ($_REQUEST['idEmployee']) {
	### получаем элемент нужного менеджера ###
	$idManager = CRestPlus::call ('lists.element.get', array(
		'IBLOCK_TYPE_ID' => 'lists', 'IBLOCK_ID' => '52',
		'FILTER' => array('PROPERTY_237' => $_REQUEST['idEmployee']),
	));
	### формируем массив с апдейтом ###
	foreach ($idManager['result'][0] as $key => $value) {
		if (preg_match("~PROPERTY_~", $key)) {
			if (!in_array($key, array('PROPERTY_241', 'PROPERTY_242', 'PROPERTY_243'))) $field[$key] = $value;
		}
	}
	$field = array_merge($field, array(
		'NAME' => 'Название', 'PROPERTY_241' => $_REQUEST['margMore'],
		'PROPERTY_242' => $_REQUEST['margLess'], 'PROPERTY_243' => $_REQUEST['margGeneral']
	));
	### апдейт ### 
	$listsUpdate = CRestPlus::call ('lists.element.update', array(
		'IBLOCK_TYPE_ID' => 'lists', 'IBLOCK_ID' => '52',
		'ELEMENT_ID'     => $idManager['result'][0]['ID'], 'FIELDS' => $field
	));
	echo json_encode($_REQUEST);
}