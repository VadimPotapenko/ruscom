<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Отчет по менеджерам</title>
		<link rel="stylesheet" type="text/css" href="view/css/style.css">
		<link href="https://fonts.googleapis.com/css?family=Montserrat+Alternates&display=swap" rel="stylesheet">
	</head>
	<body>
		<!-- фильтр -->
		<form method='post'>
			<label for='month'>Месяц:</label>
			<select id='month' name='month' value="<?=$_POST['month'];?>">
				<option value='01'>Январь</option>
				<option value='02'>Февраль</option>
				<option value='03'>Март</option>
				<option value='04'>Апрель</option>
				<option value='05'>Май</option>
				<option value='06'>Июнь</option>
				<option value='07'>Июль</option>
				<option value='08'>Август</option>
				<option value='09'>Сентябрь</option>
				<option value='10'>Октябрь</option>
				<option value='11'>Ноябрь</option>
				<option value='12'>Декабрь</option>
			</select>
			<label for='year'>Год:</label>
			<input type="text" name="year" value="<?=$_POST['year'];?>">
			<input type='submit' value='Применить' name='send' class='btn'>
		</form>

		<!-- таблица -->
		<div class='main'>
			<?php foreach ($mainArr as $k => $value): ?>
				<div class='element'>
					
					<div class="element__header">
						<button class="sendDataAboutEmployee" data-btn="test"><img src="view/images/32203.png"></button>
					</div>

					<div class="element__body">

						<div class='manager'>
							<label class='gfonts' for="manager">Менеджер:</label>
							<span class='less_right' id="manager" data-id="<?=$k; ?>"><strong><?=$value['manager'];?></strong></span>
						</div>

						<div class='plan'>
							<p>
								<span class='gfonts plan_s'>План:</span> 
								<span class='less_right'><?=formatMoney($value['plan']);?></span>
							</p>
						</div>

						<div class='out'>
							<p><span class='gfonts'>Кол-во выездов:</span> <span class='less_right'><?=$value['out'] ?: '0';?></span></p>
						</div>

						<div class='opportunity'>
							<p class='s_blocks'>
								<span class='gfonts'>Выполнено:</span> 
								<span><?=formatMoney($value['opportunity']['closed']) ?: 0;?></span>
							</p>
							<p class='s_blocks'>
								<span class='gfonts'>Первичные:</span> 
								<span><?=formatMoney($value['opportunity']['first']) ?: 0;?></span>
							</p>
							<p class='s_blocks'>
								<span class='gfonts'>Повторные:</span> 
								<span><?=formatMoney($value['opportunity']['second']) ?: 0;?></span>
							</p>
						</div>

						<div class='margin'>
							<p><span class='gfonts'>Маржинальность:</span></p>
							<span class='s_blocks'>
								<label class='gfonts' for='marg-more-<?=$k?>'> => 1,2 (%)</label>
								<input type="text" id='marg-more-<?=$k?>' class="marg-more" value="<?=$value['marg_more'];?>">
							</span>

							<span class='s_blocks'>
								<label class='gfonts space' for='marg-less-<?=$k?>'> < 1,2 (%)</label>
								<input type="text" id='marg-less-<?=$k?>' class="marg-less" value="<?=$value['marg_less'];?>">
							</span>

							<span class='s_blocks'>
								<label class='gfonts space' for='marg-general-<?=$k?>'> Общая </label>
								<input type="text" id='marg-general-<?=$k?>' class="marg-general" value="<?=$value['marg_gen'];?>">
							</span>

						</div>

						<div class='agent'>
							<p><span class='gfonts'>Агентские:</span> <span><?=formatMoney($value['agent']) ?: '0';?></span></p>
						</div>

						<div class='service'>
							<p><span class='gfonts'>Услуги:</span> <span><?=$value['services'] ?: '0';?></span></p>
						</div>

						<div class='cost_service'>
							<p>
								<span class='gfonts'>Затраты на услуги:</span> 
								<span><?=formatMoney($value['cost_services']) ?: '0';?></span>
							</p>
						</div>

						<?php if(isset($value['co_executor'])): ?>
							<div class='co-executor'>
								<p>
									<span class='gfonts'>Соисполнитель:</span> <span><?=formatMoney($value['co_executor']);?></span>
								</p>
							</div>
						<?php endif; ?>
						<?php if(isset($value['curator'])): ?>
							<div class='curator'>
								<p><span class='gfonts'>Куратор:</span> <span><?=formatMoney($value['curator']);?></span></p>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
		<script type="text/javascript" src="view/js/main.js"></script>
	</body>
</html>