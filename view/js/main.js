'use strict';

let btns = document.querySelectorAll('.sendDataAboutEmployee');

[].forEach.call(btns, btn => {
	btn.addEventListener('click', (e) => {
		let target = e.currentTarget;
		let parent = target.parentNode.parentNode;

		let data = getData(parent);
		send(data);
	});
})


/**
*	ПОЛУЧЕНИЕ ДАННЫХ ПО СОТРУДНИКУ
*	@params {node}  marginalyParent - блок маржинальности для сотрудника
*	@params {node} elementParent 	- родительский блок сотрудника
*/
function getData(parent) {
	
	let data = {};

	// МАРЖИНАЛЬНОСТЬ
	let margMore = parent.querySelector('.marg-more').value;
	let margLess = parent.querySelector('.marg-less').value;
	let margGeneral = parent.querySelector('.marg-general').value;

	// ID СОТРУДНИКА
	let idEmployee = parent.querySelector('#manager').getAttribute('data-id');

	data['margMore'] = margMore;
	data['margLess'] = margLess;
	data['margGeneral'] = margGeneral;
	data['idEmployee'] = idEmployee;

	return data;
}


/**
*	ОТПРАВКА ДАННЫХ НА БЭКЕНД
*	@params {obj}  data - объект с данным
*/
function send(data) {
	
	$.ajax({
        url:'handler.php',
        dataType: 'json',
        data: data,
        method: "POST",
        success: function(data) {
            console.log('success')
        },
        error: function(data) {
            console.log('bad answer...');
        }
    });
}