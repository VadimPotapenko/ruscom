<?php
function callMoney ($date) {
	$result = file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp?date_req='.$data);
	$xml = json_encode(simplexml_load_string($result, "SimpleXMLElement", LIBXML_NOCDATA));
	return json_decode($xml, 1);
}
function formatMoney($number, $fractional=false) {
    if ($fractional) {
        $number = sprintf('%.2f', $number);
    }
    while (true) {
        $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
        if ($replaced != $number) {
            $number = $replaced;
        } else {
            break;
        }
    }
    return $number;
}