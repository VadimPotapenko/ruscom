<?php
require (__DIR__.'/CRest.php');

class CRestPlus extends CRest {
	protected static function iteration ($method, $params) {
		$tmp = parent::call ($method, $params);
	    $iteration = intval($tmp['total'] / 50) + 1;
	    if ($tmp['total'] % 50 == 0) $iteration -= 1;
	    for ($i = 0; $i < $iteration; $i++) {
	        $start = $i * 50;
	        $data[$i]['method'] = $method;
	        $data[$i]['params'] = array('start' => $start);
	        $data[$i]['params'] += $params;
	    }
	    if (count($data) > 50) $data = array_chunk($data, 50);
	    else $data = array($data);
	    return $data;
	}

	public static function callBatchList ($method, $params) {
		$tmp = self::iteration ($method, $params);
		for ($i = 0, $s = count($tmp); $i < $s; $i++) { $result = parent::callBatch ($tmp[$i]); }
		return $result ?: 'error';
	}

	public static function callBatchUsers ($params) {
		$return = false;
		foreach ($params as $v) {
			$data[] = array('method' => 'user.get','params' => array('ID' => $v,));
		}
		$data = array_chunk($data, 50);
		for ($i = 0, $s = count($data); $i < $s; $i++) { $return = parent::callBatch ($data[$i]); }
		return $return['result'];
	}
}